# ProjectTesseract
Transdimensional combat

## Coding Formats:

Variable: **snake_case**

Functions: **snake_case**

Scenes: **PascalCase**

Folders: **PascalCase**


## Todo

- Have bullets show over networked game
- Prevent both players guns from rotating on a vertical axis during a multiplayer game
- Implement UPNP port mapping
- Autodetect running games on local network
- Show connected players in lobby screen

## Authors
- Matthias Harden
- Joe Zlonicky
- Noah Jacobsen
- Davis Carlson
